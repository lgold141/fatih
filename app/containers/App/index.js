/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import "assets/textStyle.scss";
import Footer from "components/Footer";
import Header from "components/Header";
import config from "config";
import FeaturePage from "containers/FeaturePage/Loadable";

import HomePage from "containers/HomePage/Loadable";
import NotFoundPage from "containers/NotFoundPage/Loadable";
import React from "react";
import { Helmet } from "react-helmet";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";
import "../../assets/_fonts.scss";
import "../../assets/_variable.scss";
import GlobalStyle from "../../global-styles";

// eslint-disable-next-line no-console
console.log("config--------", config);

const AppWrapper = styled.div`
  max-width: calc(768px + 16px * 2);
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  padding: 0 16px;
  flex-direction: column;
`;

export default function App() {
  return (
    <AppWrapper>
      <Helmet titleTemplate="%s - FATIH" defaultTitle="FATIH">
        <meta name="description" content="A FATIH application" />
      </Helmet>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/features" component={FeaturePage} />
        <Route path="" component={NotFoundPage} />
      </Switch>
      <Footer />
      <GlobalStyle />
    </AppWrapper>
  );
}
