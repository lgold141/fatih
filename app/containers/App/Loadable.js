/**
 * Asynchronously loads the component for HomePage
 */

import SplashScreen from "components/SplashScreen";
import React from "react";
import loadable from "utils/loadable";

export default loadable(() => import("./index"), {
  fallback: <SplashScreen/>,
});
