import { createSelector } from "reselect";
import { initialState } from "./reducer";

/**
 * Direct selector to the containerTest state domain
 */

const selectContainerTestDomain = state => state.containerTest || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ContainerTest
 */

const makeSelectContainerTest = () =>
  createSelector(
    selectContainerTestDomain,
    substate => substate,
  );

export default makeSelectContainerTest;
export { selectContainerTestDomain };
