/**
 *
 * Asynchronously loads the component for ContainerTest
 *
 */

import loadable from "utils/loadable";

export default loadable(() => import("./index"));
