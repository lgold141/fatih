/*
 * ContainerTest Messages
 *
 * This contains all the text for the ContainerTest container.
 */

import { defineMessages } from "react-intl";

export const scope = "app.containers.ContainerTest";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the ContainerTest container!",
  },
});
