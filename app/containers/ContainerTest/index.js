/**
 *
 * ContainerTest
 *
 */

import PropTypes from "prop-types";
import React, { memo } from "react";
import { Helmet } from "react-helmet";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { compose } from "redux";
import { createStructuredSelector } from "reselect";
import { useInjectReducer } from "utils/injectReducer";

import { useInjectSaga } from "utils/injectSaga";
import "./ContainerTest.scss";
import messages from "./messages";
import reducer from "./reducer";
import saga from "./saga";
import makeSelectContainerTest from "./selectors";

export function ContainerTest() {
  useInjectReducer({ key: "containerTest", reducer });
  useInjectSaga({ key: "containerTest", saga });

  return (
    <div className="container-test-wrapper">
      <Helmet>
        <title>ContainerTest</title>
        <meta name="description" content="Description of ContainerTest" />
      </Helmet>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

ContainerTest.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  containerTest: makeSelectContainerTest(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ContainerTest);
