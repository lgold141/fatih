/* eslint-disable no-console */
import axios from "axios";
import config from "config";
import queryString from "query-string";

// Set up default config for http requests here
// Please have a look at here `https://github.com/axios/axios#request- config` for the full list of configs
const axiosClient = axios.create({
  baseURL: config.baseURL,
  headers: {
    "content-type": "application/json",
  },
  paramsSerializer: params => queryString.stringify(params),
});

axiosClient.interceptors.request.use(
  axiosConfig => {
    // eslint-disable-next-line no-param-reassign
    axiosConfig.headers.Authorization = `Bearer ${localStorage.getItem(
      "token",
    )}`;
    console.log("axiosClient.interceptors.request config", axiosConfig);
    return axiosConfig;
  },
  err => {
    console.log(err);
    return Promise.reject(err);
  },
);

axiosClient.interceptors.response.use(
  response => {
    if (response && response.data) {
      return response.data;
    }

    return response;
  },
  error => {
    // Handle errors
    console.log(error);
    return Promise.reject(error);
  },
);

export default axiosClient;
