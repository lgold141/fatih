import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: Gilroy, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 14px;
  }

  body.fontLoaded {
    font-family: Gilroy, 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Gilroy, Georgia, Times, 'Times New Roman', serif;
  }
`;

export default GlobalStyle;
