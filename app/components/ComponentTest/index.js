/**
 *
 * ComponentTest
 *
 */

// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Button from "@material-ui/core/Button";
import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import "./ComponentTest.scss";
import messages from "./messages";

function ComponentTest() {
  return (
    <div className="component-test-wrapper Me">
      <Button variant="contained">Default</Button>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

ComponentTest.propTypes = {};

export default memo(ComponentTest);
