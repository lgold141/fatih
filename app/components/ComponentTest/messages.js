/*
 * ComponentTest Messages
 *
 * This contains all the text for the ComponentTest component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.ComponentTest";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the ComponentTest component!",
  },
});
