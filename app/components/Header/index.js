import React from "react";
import { FormattedMessage } from "react-intl";
import HeaderLink from "./HeaderLink";
import messages from "./messages";
import NavBar from "./NavBar";

function Header() {
  return (
    <div>
      <NavBar>
        <HeaderLink to="/">
          <FormattedMessage {...messages.home} />
        </HeaderLink>
        <HeaderLink to="/features">
          <FormattedMessage {...messages.features} />
        </HeaderLink>
      </NavBar>
    </div>
  );
}

export default Header;
