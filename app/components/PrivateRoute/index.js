/**
 *
 * PrivateRoute
 *
 */

import PropTypes from "prop-types";
import React, { memo } from "react";
import { Redirect, Route } from "react-router-dom";
// eslint-disable-next-line import/extensions
import { authenticationService } from "service/authentication.service.js";

// import styled from 'styled-components';
function PrivateRoute({ component: Component, roles, ...rest }) {
  return (
    <Route
      {...rest}
      render={props => {
        const currentUser = authenticationService.currentUserValue;
        if (!currentUser) {
          // not logged in so redirect to login page with the return url
          return (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          );
        }

        // check if route is restricted by role
        if (roles && roles.indexOf(currentUser.role) === -1) {
          // role not authorised so redirect to home page
          return <Redirect to={{ pathname: "/" }}/>;
        }

        // authorised so return component
        return <Component {...props} />;
      }}
    />
  );
}

PrivateRoute.propTypes = {
  roles: PropTypes.any,
  component: PropTypes.any,
  location: PropTypes.any,
};

PrivateRoute.defaultProps = {};

export default memo(PrivateRoute);
