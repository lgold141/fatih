/**
 *
 * FormHook
 *
 */

import ClassNames from "classnames";
import PropTypes from "prop-types";
import React, { memo } from "react";
import { useForm } from "react-hook-form";
import "./FormHook.scss";

function FormHook({ children, theme, defaultValues, onSubmit }) {
  const methods = useForm({ defaultValues });
  const { handleSubmit } = methods;

  return (
    <form className={ClassNames("form-hook-wrapper", theme)} onSubmit={handleSubmit(onSubmit)}>
      {Array.isArray(children)
        ? children.map(child => {
          return child.props.name
            ? React.createElement(child.type, {
              ...{
                ...child.props,
                register: methods.register,
                key: child.props.name,
              },
            })
            : child;
        })
        : children}
    </form>
  );
}

FormHook.propTypes = {
  theme: PropTypes.string,
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.any,
  children: PropTypes.any,
};
FormHook.defaultProps = {
  theme: "white",
  onSubmit: data => console.log(data),
};
export default memo(FormHook);
