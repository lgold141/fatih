/*
 * TextInputControl Messages
 *
 * This contains all the text for the TextInputControl component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.TextInputControl";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the TextInputControl component!",
  },
});
