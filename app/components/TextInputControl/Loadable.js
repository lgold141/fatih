/**
 *
 * Asynchronously loads the component for TextInputControl
 *
 */

import loadable from "utils/loadable";

export default loadable(() => import("./index"));
