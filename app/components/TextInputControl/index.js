/**
 *
 * TextInputControl
 *
 */

import ClassNames from "classnames";
import React, { memo } from "react";
import PropTypes from "prop-types";
import { TextField } from "@material-ui/core";
import "./TextInputControl.scss";

function TextInputControl({
  label,
  id,
  name,
  type,
  disabled,
  error,
  defaultValue,
  fullWidth,
  multiline,
  locked,
  iconIsValid: IconIsValid,
  confirmButton: ConfirmButton,
  appendComponent: AppendComponent,
  handleChange,
}) {
  return (
    <div className="text-input-control-wrapper">
      <TextField
        id={id}
        name={name}
        type={type}
        disabled={disabled}
        error={error}
        defaultValue={defaultValue}
        fullWidth={fullWidth}
        multiline={multiline}
        className={ClassNames(`text-input-control ${locked ? "locked" : ""}`)}
        label={label}
        variant="filled"
        InputProps={{ disableUnderline: true }}
        onChange={handleChange}
      />
      <div className="right-component-wrapper">
        {ConfirmButton ? <ConfirmButton /> : ""}
        {IconIsValid ? <IconIsValid /> : ""}
      </div>
      <div className="append-component-wrapper">
        {AppendComponent ? <AppendComponent /> : ""}
      </div>
    </div>
  );
}

TextInputControl.propTypes = {
  label: PropTypes.string, // as the same with placeholder
  id: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  defaultValue: PropTypes.string,
  fullWidth: PropTypes.bool,
  multiline: PropTypes.bool,
  locked: PropTypes.bool, // different with disabled. for example, a field will be lock when user click on confirm email or password button
  iconIsValid: PropTypes.any,
  confirmButton: PropTypes.any,
  appendComponent: PropTypes.any, // ex: password strength
  handleChange: PropTypes.func,
};

export default memo(TextInputControl);
