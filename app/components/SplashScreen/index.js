/**
 *
 * SplashScreen
 *
 */

// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import logo from "assets/images/inverted.svg";
import React from "react";

import "./SplashScreen.scss";

function SplashScreen() {
  return (
    <div className="splash-screen-wrapper d-flex justify-content-center">
      <div className="justify-content-center align-self-center">
        <img alt="splash" src={logo}/>
      </div>
    </div>
  );
}

SplashScreen.propTypes = {};

SplashScreen.defaultProps = {};

export default SplashScreen;
