## Steps to build
##### Step 1: Clone this repository and navigate into project folder

##### Step 2: Installing dependencies, run this command
```
npm install
```
or
```
yarn install
```
##### Step 3:
-  If you want to run this website on localhost, run this command:

```
npm start
```

The website is hosted at `http://localhost:3000` is default

Config port in `server\port.js`

## Step to deploy 

- If you host this website in somewhere on server, follow the following steps:
1. Update `app/config.dev.json` for `config.prod.json`, depend on which environment you want to deploy.
 

```
  "baseURL": ""
```

Command line to build
- For development
```
npm run build:deploy:dev
```
- For production
```
npm run build:deploy:production
```
The build files are served at `<root>/build` folder
